<div class="container">
    <div class="row">
        <div class="col">
            <div class="main-post">
                <h3 class="main-title">
                    this is our main title post
                </h3>

                <span class="post-author">author</span>
                <span class="post-date">30/03/2022</span>
                <span class="post-comments">comments</span>
                <img src="http://placehold.it/300x100/300" alt="">
                <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Dignissimos, assumenda?</p>
                <hr>
                <p class="categories">html testing </p>
            </div>
        </div>
    </div>
</div>


<?php the_field('wysiwug'); ?>
<?php the_field('video-showcase'); ?>