<section class="parallex ">
    <div class="container_fluid">
        <div class="row">
            <div class="col d-flex">
                <div class="parallex-text">
                    <div class="parallex-content d-flex justify-content-center align-items-center w-100 h-100">
                      <h2 class="h2 text-center ">Lorem ipsum dolor sit amet consectetur, adipisicing elit. Illum, vero?</h2>  
                    </div>
                </div>
                <div class="parallex-img">
                    <img class="img img-responsive" src="https://picsum.photos/seed/picsum/400/400" alt="">
                </div>
            </div>
        </div>
    </div>
</section>