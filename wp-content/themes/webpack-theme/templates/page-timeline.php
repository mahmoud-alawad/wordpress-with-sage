<section class="timeline ">
    <div class="container">
        <div class="row py-100">
            <div class="col-md-3">
                <div class="timeline-block">
                    <h1 class="timeline-block-title" data-title="80">80</h1>
                    <p class="timeline-block-description">Lorem ipsum dolor sit amet consectetur adipisicing elit.</p>
                </div>
            </div>
            <div class="col-md-3">
                <div class="timeline-block">
                    <h1 class="timeline-block-title"  data-title="80">80</h1>
                    <p class="timeline-block-description"  data-title="80">Lorem ipsum dolor sit amet consectetur adipisicing elit.</p>
                </div>
            </div>
            <div class="col-md-3">
                <div class="timeline-block">
                    <h1 class="timeline-block-title"  data-title="80">80</h1>
                    <p class="timeline-block-description">Lorem ipsum dolor sit amet consectetur adipisicing elit.</p>
                </div>
            </div>
            <div class="col-md-3">
                <div class="timeline-block">
                    <h1 class="timeline-block-title"  data-title="80">80</h1>
                    <p class="timeline-block-description">Lorem ipsum dolor sit amet consectetur adipisicing elit.</p>
                </div>
            </div>
        </div>
    </div>
</section>