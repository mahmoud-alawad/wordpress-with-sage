import anime from 'animejs';

export default (element, params = {}, stopAnimation = {}) => {
  let content = $(element);
  let contentPosition = content[0].getBoundingClientRect().top;
  let windowHeight = window.innerHeight;
  if (Math.ceil(contentPosition) < Math.ceil(windowHeight)) {
    let animation = anime({
      targets: element,

      duration: 1000,
      ...params,
    });
  } else {
    anime({
      targets: element,
      duration: 1000,
      ...stopAnimation,
    });
  }
};
