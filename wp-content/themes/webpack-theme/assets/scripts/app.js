/**
 * <mahmoud alawad : awad25.ma@gmail.com>
 *
 * @format
 * @auther
 */

import swiper from './swiper.js';
import '../style/app.scss';
import anime from 'animejs';
import Header from './header';
import scroll from './scroll';

const initHeader = () => {
  $('.main-carousel').flickity({
    // options
    imagesLoaded: true,
    percentPosition: false,
    cellAlign: 'left',
    contain: true,
    draggable: true,
  });

  let dots = $('.flickity-page-dots .flickity-page-dot');
  let dotsArray = [...$('.slider-dot-hidden')];

  dots.map((index, item) => {
    let data = dotsArray[index];
    item.innerText = data.dataset.title;
  });

  anime({
    targets: '.main-carousel-cell-cta',
    opacity: 1,
    translateX: '-50%',
    rotate: '1turn',
    delay: 300,
  });
};

$(document).ready(function () {
  $(window).scroll(function () {});
  $('.navbar-nav .nav-item a').each(function (i, item) {
    console.log(item);
    $(item).attr('class', 'nav-link');
  });
  initHeader();
  let timelineTextOptions = {
    prop1: 0,
    prop2: 0,
  };
  let timelineAnime = $('.timeline-block-title').each((i, item) => {
    item.classList.add('anime' + i);
    anime('.timeline-block-title anime-1', {
      easing: 'linear',
      round: 1,
      value: [0, 1000],
      easing: 'easeInOutExpo',
    });
  });

  $(window).on('scroll', function (e) {
    scroll(
      '.intro-home',
      { opacity: 1, scale: 1, duration: 2000 },
      { opacity: 0, scale: 0 },
    );
  });
});
