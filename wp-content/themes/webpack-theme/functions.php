<?php


// Adds dynamic title tag support
function title_theme_support()
{
    # code...
    add_theme_support('title-tag');   
    add_theme_support( 'custom-logo', array(
        'height' => 480,
        'width'  => 720,
    ) );
}

add_action('‘after_setup_theme','title_theme_support');


//menus 

function register_menus()
{
    $location = array(
        'primary'=> "description primary",
        'footer' => "footer menu items"
    );

    register_nav_menus($location);
}

add_action('init','register_menus');

function register_style(){
    $version = wp_get_theme()->get('Version')  || 4;
    wp_enqueue_style('register-bootstrap', "https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css",array(),  $version ,'all');
    wp_enqueue_style('register-fontawesome', "https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css",array(),  $version ,'all');
    wp_enqueue_style('register-flickity', "https://unpkg.com/flickity@2/dist/flickity.min.css",array(),  $version ,'all');
    wp_enqueue_style('register-style',     get_template_directory_uri() . '/dist/app.css', array(), filemtime(get_template_directory() . '/dist/app.css'), false
);
}
add_action('wp_enqueue_scripts', 'register_style');



function register_scripts(){
    $version = wp_get_theme()->get('Version')  || 4;

    wp_enqueue_script('register-jquery', "https://code.jquery.com/jquery-3.4.1.slim.min.js",array(),'3.4.1',true );
    wp_enqueue_script('register-popper', "https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js",array(),'3.4.1',true );
    wp_enqueue_script('register-bootstrap', "https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js",array(),'3.4.1',true );
    wp_enqueue_script('register-flickity', "https://unpkg.com/flickity@2/dist/flickity.pkgd.min.js",array(),'3.4.1',true );

    wp_enqueue_script('register-main',get_template_directory_uri() . "/dist/app.js",array(), '1.2',true);

}
add_action('wp_enqueue_scripts', 'register_scripts');
add_theme_support( 'post-thumbnails' );





function my_first_post_type (){
    $args = array(
        'labels'=> array(
            'name' => 'videos',
            'singular_name' => 'video'
        ),
        'hierarchical' => true,
        'public' => true,
        'has_archive' => true,
        'supports' =>   array('title','editor','thumbnail'),
        'menu_icon' => 'dashicons-video-alt3'
    );
   
    register_post_type('videos',$args);

}

add_action('init','my_first_post_type');

    //post thumbnails
add_theme_support('post-thumbnails');

?>
