<?php

if (have_posts()) {
	while(have_posts()){
		the_post();
    }
}
?>

<div class="video">
    <?php if(has_post_thumbnail()): ?>
        <img class="img img-responsive" src="<?php the_post_thumbnail_url('blog-large'); ?>" alt="<? the_title(); ?>">
    <?php endif;?>
    <h1><? the_title(); ?></h1>
</div>

