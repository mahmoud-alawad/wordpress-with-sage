<!DOCTYPE html>
<html  class="no-js" <?php language_attributes(); ?>> 
<head>
    <!-- Meta -->
    <meta charset=<?php echo(bloginfo("charset"));?>>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Blog Site Template">
    <meta name="author" content="https://github.com/mahmoud-alawad">    
    <link rel="shortcut icon" href="images/logo.png"> 
    
    
<?php 
	wp_head();
?>
</head> 

<body>
    
    <header class=" text-center">	   
    <div class="overlay"></div>

       <?php
         
         $custom_logo_id = get_theme_mod( 'custom_logo' );
         $logo = wp_get_attachment_image_src( $custom_logo_id , 'full' );
          
         if ( has_custom_logo() ) {
             echo '<img src="' . esc_url( $logo[0] ) . '" alt="' . get_bloginfo( 'name' ) . '">';
         } 
        ?>

        <nav class="navbar navbar-expand-lg">
        <div class="container d-flex justif-content-center">
        <?php
                wp_nav_menu(
                    array(
                        'menu' => 'primary',
                        'container' => '',
                        'theme_location' => 'primary',
                        'menu_class'           => 'menu navbar-nav m-auto',
                    )
                );
                ?>    
        </div>    
</nav>
    </header>
    <div class="main-wrapper position-relative">
	   