/** @format */

module.exports = {
  bracketSameLine: true,
  singleQuote: true,
  trailingComma: 'all',
  arrowParens: 'avoid',
  insertPragma: true,
};
