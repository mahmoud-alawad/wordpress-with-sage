const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const BrowserSyncPlugin = require('browser-sync-webpack-plugin');
let path = require('path');

// change these variables to fit your project
const jsPath = './assets/scripts';
const sassPath = './assets/style';
const outputPath = 'dist';
const localDomain = 'http://wpsite.local';
const entryPoints = {
  // you can have more than 1 entry point
  app: jsPath + '/app.js',
};

module.exports = {
  entry: entryPoints,
  output: {
    path: path.resolve(__dirname, outputPath),
    filename: '[name].js',
  },
  plugins: [
    new MiniCssExtractPlugin({
      filename: '[name].css',
    }),

    new BrowserSyncPlugin(
      {
        proxy: localDomain,
        files: [outputPath + '/*.css'],
        injectCss: true,
      },
      { reload: false },
    ),
  ],
  module: {
    rules: [
      {
        test: /\.s?[c]ss$/i,
        use: [MiniCssExtractPlugin.loader, 'css-loader', 'sass-loader'],
      },
      {
        test: /\.sass$/i,
        use: [
          MiniCssExtractPlugin.loader,
          'css-loader',
          {
            loader: 'sass-loader',
            options: {
              sassOptions: { indentedSyntax: true },
            },
          },
        ],
      },
      {
        test: /\.(jpg|jpeg|png|gif|woff|woff2|eot|ttf|svg)$/i,
        use: 'url-loader?limit=1024',
      },
    ],
  },
};
