<?php

// Adds dynamic title tag support
function title_theme_support()
{
    # code...
    add_theme_support('title-tag');   
    add_theme_support( 'custom-logo', array(
        'height' => 480,
        'width'  => 720,
    ) );

}

add_action('‘after_setup_theme','title_theme_support');


//menus 

function register_menus()
{
    $location = array(
        'primary'=> "description primary",
        'footer' => "footer menu items"
    );

    register_nav_menus($location);
}

add_action('init','register_menus');

function register_style(){
    $version = wp_get_theme()->get('Version');
    wp_enqueue_style('register-bootstrap', "https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css",array(),  $version ,'all');
    wp_enqueue_style('register-fontawesome', "https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css",array(),  $version ,'all');
    wp_enqueue_style('register-style', get_template_directory_uri() . "/style.css",array('register-bootstrap'),  $version ,'all');

}
add_action('wp_enqueue_scripts', 'register_style');



function register_scripts(){

    wp_enqueue_script('register-jquery', "https://code.jquery.com/jquery-3.4.1.slim.min.js",array(),'3.4.1',true );
    wp_enqueue_script('register-anime', "https://cdnjs.cloudflare.com/ajax/libs/animejs/3.2.1/anime.min.js",array(),'3.4.3',true );
    wp_enqueue_script('register-popper', "https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js",array(),'3.4.1',true );
    wp_enqueue_script('register-bootstrap', "https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js",array(),'3.4.1',true );
    wp_enqueue_script('register-main',get_template_directory_uri() . "/assets/js/main.js",array(),'1.0',true);

}
add_action('wp_enqueue_scripts', 'register_scripts');
?>