<!DOCTYPE html>
<html  class="no-js" <?php language_attributes(); ?>> 
<head>
    <!-- Meta -->
    <meta charset=<?php echo(bloginfo("charset"));?>>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Blog Site Template">
    <meta name="author" content="https://github.com/mahmoud-alawad">    
    <link rel="shortcut icon" href="images/logo.png"> 
    
    
<?php 
	wp_head();
?>
</head> 

<body>
    
    <header class="header text-center">	    
       <?php
         
         $custom_logo_id = get_theme_mod( 'custom_logo' );
         $logo = wp_get_attachment_image_src( $custom_logo_id , 'full' );
          
         if ( has_custom_logo() ) {
             echo '<img src="' . esc_url( $logo[0] ) . '" alt="' . get_bloginfo( 'name' ) . '">';
         } else {
             echo '<h1>' . get_bloginfo('name') . '</h1>';
         }
        ?>

        <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <div class="container-fluid d-flex justify-content-between">
                <?php
                wp_nav_menu(
                    array(
                        'menu' => 'primary',
                        'ontainer' => '',
                        'theme_location' => 'primary',
                        'menu_class' => 'nav',
                    )
                );
                ?>        
         </div>
</nav>
    </header>
    <div class="main-wrapper">
	    <header class="page-title theme-bg-light text-center gradient py-5">
			<h1 class="heading">Blog Home Page Heading</h1>
		</header>