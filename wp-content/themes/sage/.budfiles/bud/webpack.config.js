module.exports = {
  "bail": false,
  "cache": {
    "name": "bud.development",
    "type": "filesystem",
    "version": "ijmi9cf09bjiybz7_e0e3rd9twc_",
    "cacheDirectory": "/home/mahmoud/Desktop/wordpress/wordpress-with-sage/wp-content/themes/sage/.budfiles/cache/webpack",
    "managedPaths": [
      "/home/mahmoud/Desktop/wordpress/wordpress-with-sage/wp-content/themes/sage/node_modules"
    ],
    "buildDependencies": {
      "bud": [
        "/home/mahmoud/Desktop/wordpress/wordpress-with-sage/wp-content/themes/sage/package.json",
        "/home/mahmoud/Desktop/wordpress/wordpress-with-sage/wp-content/themes/sage/.editorconfig",
        "/home/mahmoud/Desktop/wordpress/wordpress-with-sage/wp-content/themes/sage/bud.config.js",
        "/home/mahmoud/Desktop/wordpress/wordpress-with-sage/wp-content/themes/sage/composer.json",
        "/home/mahmoud/Desktop/wordpress/wordpress-with-sage/wp-content/themes/sage/jsconfig.json",
        "/home/mahmoud/Desktop/wordpress/wordpress-with-sage/wp-content/themes/sage/tailwind.config.js",
        "/home/mahmoud/Desktop/wordpress/wordpress-with-sage/wp-content/themes/sage/theme.json"
      ]
    }
  },
  "context": "/home/mahmoud/Desktop/wordpress/wordpress-with-sage/wp-content/themes/sage",
  "devtool": false,
  "infrastructureLogging": {
    "console": {
      "Console": {}
    }
  },
  "mode": "development",
  "module": {
    "noParse": {},
    "rules": [
      {
        "test": {},
        "include": [
          "/home/mahmoud/Desktop/wordpress/wordpress-with-sage/wp-content/themes/sage/resources"
        ],
        "parser": {
          "requireEnsure": false
        }
      },
      {
        "oneOf": [
          {
            "test": {},
            "use": [
              {
                "loader": "/home/mahmoud/Desktop/wordpress/wordpress-with-sage/wp-content/themes/sage/node_modules/babel-loader/lib/index.js",
                "options": {
                  "presets": [
                    [
                      "/home/mahmoud/Desktop/wordpress/wordpress-with-sage/wp-content/themes/sage/node_modules/@babel/preset-env/lib/index.js"
                    ],
                    [
                      "/home/mahmoud/Desktop/wordpress/wordpress-with-sage/wp-content/themes/sage/node_modules/@babel/preset-react/lib/index.js"
                    ]
                  ],
                  "plugins": [
                    [
                      "/home/mahmoud/Desktop/wordpress/wordpress-with-sage/wp-content/themes/sage/node_modules/@babel/plugin-transform-runtime/lib/index.js",
                      {
                        "helpers": false
                      }
                    ],
                    [
                      "/home/mahmoud/Desktop/wordpress/wordpress-with-sage/wp-content/themes/sage/node_modules/@babel/plugin-proposal-object-rest-spread/lib/index.js"
                    ],
                    [
                      "/home/mahmoud/Desktop/wordpress/wordpress-with-sage/wp-content/themes/sage/node_modules/@babel/plugin-syntax-dynamic-import/lib/index.js"
                    ],
                    [
                      "/home/mahmoud/Desktop/wordpress/wordpress-with-sage/wp-content/themes/sage/node_modules/@babel/plugin-proposal-class-properties/lib/index.js"
                    ]
                  ]
                }
              }
            ],
            "include": [
              "/home/mahmoud/Desktop/wordpress/wordpress-with-sage/wp-content/themes/sage/resources"
            ]
          },
          {
            "test": {},
            "use": [
              {
                "loader": "/home/mahmoud/Desktop/wordpress/wordpress-with-sage/wp-content/themes/sage/node_modules/style-loader/dist/cjs.js"
              },
              {
                "loader": "/home/mahmoud/Desktop/wordpress/wordpress-with-sage/wp-content/themes/sage/node_modules/css-loader/dist/cjs.js",
                "options": {
                  "importLoaders": 1,
                  "sourceMap": false
                }
              },
              {
                "loader": "/home/mahmoud/Desktop/wordpress/wordpress-with-sage/wp-content/themes/sage/node_modules/postcss-loader/dist/cjs.js",
                "options": {
                  "postcssOptions": {
                    "plugins": [
                      [
                        "/home/mahmoud/Desktop/wordpress/wordpress-with-sage/wp-content/themes/sage/node_modules/postcss-import/index.js"
                      ],
                      [
                        null
                      ],
                      [
                        null
                      ],
                      [
                        "/home/mahmoud/Desktop/wordpress/wordpress-with-sage/wp-content/themes/sage/node_modules/postcss-preset-env/dist/index.cjs",
                        {
                          "stage": 1,
                          "features": {
                            "focus-within-pseudo-class": false
                          }
                        }
                      ]
                    ]
                  },
                  "sourceMap": true
                }
              }
            ],
            "include": [
              "/home/mahmoud/Desktop/wordpress/wordpress-with-sage/wp-content/themes/sage/resources"
            ]
          },
          {
            "test": {},
            "use": [
              {
                "loader": "/home/mahmoud/Desktop/wordpress/wordpress-with-sage/wp-content/themes/sage/node_modules/style-loader/dist/cjs.js"
              },
              {
                "loader": "/home/mahmoud/Desktop/wordpress/wordpress-with-sage/wp-content/themes/sage/node_modules/css-loader/dist/cjs.js",
                "options": {
                  "importLoaders": 1,
                  "localIdentName": "[name]__[local]___[hash:base64:5]",
                  "modules": true,
                  "sourceMap": false
                }
              }
            ],
            "include": [
              "/home/mahmoud/Desktop/wordpress/wordpress-with-sage/wp-content/themes/sage/resources"
            ]
          },
          {
            "test": {},
            "include": [
              "/home/mahmoud/Desktop/wordpress/wordpress-with-sage/wp-content/themes/sage/resources"
            ],
            "type": "asset/resource",
            "generator": {
              "filename": "images/[name][ext]"
            }
          },
          {
            "test": {},
            "include": [
              "/home/mahmoud/Desktop/wordpress/wordpress-with-sage/wp-content/themes/sage/resources"
            ],
            "type": "asset/resource",
            "generator": {
              "filename": "images/[name][ext]"
            }
          },
          {
            "test": {},
            "include": [
              "/home/mahmoud/Desktop/wordpress/wordpress-with-sage/wp-content/themes/sage/resources"
            ],
            "type": "asset/resource",
            "generator": {
              "filename": "images/[name][ext]"
            }
          },
          {
            "test": {},
            "include": [
              "/home/mahmoud/Desktop/wordpress/wordpress-with-sage/wp-content/themes/sage/resources"
            ],
            "type": "asset",
            "generator": {
              "filename": "fonts/[name][ext]"
            }
          },
          {
            "test": {},
            "include": [
              "/home/mahmoud/Desktop/wordpress/wordpress-with-sage/wp-content/themes/sage/resources"
            ],
            "type": "json",
            "parser": {}
          },
          {
            "test": {},
            "include": [
              "/home/mahmoud/Desktop/wordpress/wordpress-with-sage/wp-content/themes/sage/resources"
            ],
            "type": "json",
            "parser": {}
          },
          {
            "test": {},
            "use": [
              {
                "loader": "/home/mahmoud/Desktop/wordpress/wordpress-with-sage/wp-content/themes/sage/node_modules/html-loader/dist/cjs.js"
              }
            ],
            "include": [
              "/home/mahmoud/Desktop/wordpress/wordpress-with-sage/wp-content/themes/sage/resources"
            ]
          },
          {
            "test": {},
            "use": [
              {
                "loader": "/home/mahmoud/Desktop/wordpress/wordpress-with-sage/wp-content/themes/sage/node_modules/csv-loader/index.js"
              }
            ],
            "include": [
              "/home/mahmoud/Desktop/wordpress/wordpress-with-sage/wp-content/themes/sage/resources"
            ]
          },
          {
            "test": {},
            "use": [
              {
                "loader": "/home/mahmoud/Desktop/wordpress/wordpress-with-sage/wp-content/themes/sage/node_modules/xml-loader/index.js"
              }
            ],
            "include": [
              "/home/mahmoud/Desktop/wordpress/wordpress-with-sage/wp-content/themes/sage/resources"
            ]
          },
          {
            "test": {},
            "include": [
              "/home/mahmoud/Desktop/wordpress/wordpress-with-sage/wp-content/themes/sage/resources"
            ],
            "type": "json",
            "parser": {}
          }
        ]
      }
    ],
    "unsafeCache": false
  },
  "name": "bud",
  "node": false,
  "output": {
    "assetModuleFilename": "[name][ext]",
    "chunkFilename": "[name].js",
    "filename": "[name].js",
    "path": "/home/mahmoud/Desktop/wordpress/wordpress-with-sage/wp-content/themes/sage/public",
    "pathinfo": false,
    "publicPath": "/"
  },
  "optimization": {
    "emitOnErrors": false,
    "minimize": false,
    "minimizer": [
      "..."
    ],
    "splitChunks": {
      "cacheGroups": {
        "bud": {
          "chunks": "all",
          "test": {},
          "reuseExistingChunk": true,
          "priority": -10
        },
        "vendor": {
          "chunks": "all",
          "test": {},
          "reuseExistingChunk": true,
          "priority": -20
        }
      }
    }
  },
  "parallelism": 11,
  "performance": {
    "hints": false
  },
  "recordsPath": "/home/mahmoud/Desktop/wordpress/wordpress-with-sage/wp-content/themes/sage/.budfiles/bud/modules.json",
  "stats": "normal",
  "target": "browserslist:/home/mahmoud/Desktop/wordpress/wordpress-with-sage/wp-content/themes/sage/package.json",
  "plugins": [
    {
      "patterns": [
        {
          "from": "images/**/*",
          "context": "/home/mahmoud/Desktop/wordpress/wordpress-with-sage/wp-content/themes/sage/resources",
          "noErrorOnMissing": true
        }
      ],
      "options": {}
    },
    {
      "options": {}
    },
    {
      "options": {
        "assetHookStage": null,
        "basePath": "",
        "fileName": "manifest.json",
        "filter": null,
        "map": null,
        "publicPath": "/",
        "removeKeyHash": {},
        "sort": null,
        "transformExtensions": {},
        "useEntryKeys": false,
        "useLegacyEmit": false,
        "writeToFileEmit": true
      }
    },
    {
      "options": {
        "emitHtml": false,
        "publicPath": ""
      },
      "plugin": {
        "name": "EntrypointsManifestPlugin",
        "stage": null
      },
      "name": "entrypoints.json"
    },
    {
      "name": "WordPressExternalsWebpackPlugin",
      "stage": null,
      "externals": {
        "type": "window"
      }
    },
    {
      "plugin": {
        "name": "MergedManifestPlugin"
      },
      "file": "entrypoints.json",
      "entrypointsName": "entrypoints.json",
      "wordpressName": "wordpress.json"
    },
    {
      "plugin": {
        "name": "WordPressDependenciesWebpackPlugin",
        "stage": null
      },
      "manifest": {},
      "usedDependencies": {},
      "fileName": "wordpress.json"
    }
  ],
  "entry": {
    "app": {
      "import": [
        "@roots/bud-server/client/index.js?name=bud&bud.overlay=true&bud.indicator=true&path=/__hmr",
        "@roots/bud-server/client/proxy-click-interceptor.js",
        "@scripts/app",
        "@styles/app"
      ]
    },
    "editor": {
      "import": [
        "@roots/bud-server/client/index.js?name=bud&bud.overlay=true&bud.indicator=true&path=/__hmr",
        "@roots/bud-server/client/proxy-click-interceptor.js",
        "@scripts/editor",
        "@styles/editor"
      ]
    }
  },
  "resolve": {
    "alias": {
      "@src": "/home/mahmoud/Desktop/wordpress/wordpress-with-sage/wp-content/themes/sage/resources",
      "@dist": "/home/mahmoud/Desktop/wordpress/wordpress-with-sage/wp-content/themes/sage/public",
      "@fonts": "/home/mahmoud/Desktop/wordpress/wordpress-with-sage/wp-content/themes/sage/resources/fonts",
      "@images": "/home/mahmoud/Desktop/wordpress/wordpress-with-sage/wp-content/themes/sage/resources/images",
      "@scripts": "/home/mahmoud/Desktop/wordpress/wordpress-with-sage/wp-content/themes/sage/resources/scripts",
      "@styles": "/home/mahmoud/Desktop/wordpress/wordpress-with-sage/wp-content/themes/sage/resources/styles"
    },
    "extensions": [
      ".wasm",
      ".mjs",
      ".js",
      ".jsx",
      ".css",
      ".json",
      ".toml",
      ".yml"
    ],
    "modules": [
      "/home/mahmoud/Desktop/wordpress/wordpress-with-sage/wp-content/themes/sage/resources",
      "node_modules"
    ]
  }
}