**clone repo to your pc and run**
1. you need to have docker installed and working on your macchine
```
 docker-compose up

```

you can access wp on [localhost:8000](http://localhost:8000/)

<p align="center">
  <a href="https://somewhere">
    <img alt="Sage" src="./logotheme.png" height="100">
  </a>
</p>
<p align="center">
  <a href="LICENSE.md">
    <img alt="MIT License" src="https://img.shields.io/github/license/roots/sage?color=%23525ddc&style=flat-square" />
  </a>
  </p>
  <a href="https://somewhere/"><strong><code>Website</code></strong></a> 
 
